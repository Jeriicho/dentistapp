import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddAppointmentComponent} from "./appointment/add-appointment.component";
import {DentistappRoutes} from "./dentistapp-routes";
import {AppointmentListComponent} from "./appointment/appointment-list.component";


const routes: Routes = [
  { path: DentistappRoutes.newBookingRoute(), component: AddAppointmentComponent },
  { path: DentistappRoutes.bookingsRoute(), component: AppointmentListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class DentistappRoutingModule { }
