import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {DentistappRoutes} from "../dentistapp-routes";

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  constructor(private router: Router) {
  }

  navigateToBooking(): void {
    this.router.navigate(['/' + DentistappRoutes.bookingsRoute()]);
  }

  navigateToNewBooking(): void {
    this.router.navigate(['/' + DentistappRoutes.newBookingRoute()]);
  }

  bookingRoute(): string {
    return '/' + DentistappRoutes.bookingsRoute();
  }

  newBookingRoute(): string {
    return '/' + DentistappRoutes.newBookingRoute();
  }

}
