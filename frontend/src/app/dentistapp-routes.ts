export class DentistappRoutes {

  static bookingsRoute(): string {
    return 'bookings';
  }

  static newBookingRoute(): string {
    return 'bookings/add'
  }

}
