import {Repository} from "../repository";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {DentistVisit} from "./dentist-visit";

@Injectable()
export class DentistVisitRepository extends Repository {

  protected url = 'rest/dentist-visit';

  constructor(private http: HttpClient) {
    super();
  }

  findAll(): Promise<DentistVisit[]> {
    return this.http.get<DentistVisit[]>(`${this.url}`).toPromise();
  }

  create(dentistVisit: DentistVisit): Promise<any> {
    return this.http.post(this.url, dentistVisit).toPromise();
  }

  edit(dentistVisit: DentistVisit): Promise<any> {
    return this.http.put(`${this.url}/${dentistVisit.id}`, dentistVisit).toPromise();
  }

  delete(dentistVisit: DentistVisit): Promise<any> {
    return this.http.delete(`${this.url}/${dentistVisit.id}`).toPromise();
  }

}
