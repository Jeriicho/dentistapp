import {Dentist} from "../dentist/dentist";
import * as moment from "moment";
import {MomentUtil} from "../util/moment-util";

interface DentistVisitInput {
  id: number | null | undefined;
  dentist: Dentist | undefined;
  visitDateTime: string | undefined;
}

export class DentistVisit {

  constructor(input?: DentistVisitInput) {
    this.id = input && input.id || undefined;
    this.dentist = input && input.dentist || undefined;
    this.visitDateTime = input && input.visitDateTime
      ? MomentUtil.formatDate(moment(input.visitDateTime))
      : undefined;
  }

  id: number = undefined;
  dentist: Dentist = undefined;
  visitDateTime: string = undefined;


}
