import {DentistVisit} from "../dentist_visit/dentist-visit";
import * as moment from "moment";
import {Moment} from "moment";

export class DentistVisitItem {

  constructor(dentistVisit: DentistVisit) {
    this.dentistVisit = dentistVisit;
    this.dentistVisit.visitDateTime = dentistVisit.visitDateTime;
    this.date = moment(dentistVisit.visitDateTime);
    this.time = moment(dentistVisit.visitDateTime).format("HH:mm");
  }

  dentistVisit: DentistVisit;
  date: Moment;
  time: string;

}
