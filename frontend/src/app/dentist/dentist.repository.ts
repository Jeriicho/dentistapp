import {Injectable} from "@angular/core";
import {Repository} from "../repository";
import {HttpClient} from "@angular/common/http";
import {Dentist} from "./dentist";
import {DentistVisit} from "../dentist_visit/dentist-visit";

@Injectable()
export class DentistRepository extends Repository {

  protected url = 'rest/dentist';

  constructor(private http: HttpClient) {
    super();
  }

  findAll(): Promise<Dentist[]> {
    return this.http.get<Dentist[]>(`${this.url}`).toPromise();
  }

  existsOverlappingVisit(dentistVisit: DentistVisit): Promise<Boolean> {
    return this.http
      .get<Boolean>(`${this.url}/${dentistVisit.dentist.id}/exists-overlap`,
        {params: { visitDateTime: dentistVisit.visitDateTime }})
      .toPromise();
  }
}
