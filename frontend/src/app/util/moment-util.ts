import {Moment} from "moment";
import * as moment from 'moment';

export class MomentUtil {

  static joinDateAndTime(date: string, time: string): string {
    return moment(date + 'T' + time).format('YYYY-MM-DDTHH:MM');
  }

  static formatDate(dateTime: Moment) {
    return dateTime.format('YYYY-MM-DDTHH:mm');
  }

  static setHoursAndMinutes(dateTime: Moment, time: string): Moment {
    const hoursAndMinutes: string[] = time.split(':');
    dateTime.set('hour', parseInt(hoursAndMinutes[0], 10));
    dateTime.set('minute', parseInt(hoursAndMinutes[1], 10));
    return dateTime;
  }

}
