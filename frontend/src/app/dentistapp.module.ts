import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {DentistappRoutingModule} from './dentistapp-routing.module';
import {DentistappComponent} from './dentistapp.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from "@angular/common/http";
import {AppointmentModule} from "./appointment/appointment.module";
import {NavbarModule} from "./navbar/navbar.module";
import {MatMomentDateModule} from "@angular/material-moment-adapter";
import {NotificationsService, SimpleNotificationsModule} from "angular2-notifications";

@NgModule({
  declarations: [
    DentistappComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppointmentModule,
    NavbarModule,
    MatMomentDateModule,
    SimpleNotificationsModule.forRoot(),
    DentistappRoutingModule
  ],
  providers: [
    NotificationsService
  ],
  bootstrap: [DentistappComponent]
})
export class DentistappModule {
}
