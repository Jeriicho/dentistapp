import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {DentistappRoutes} from "./dentistapp-routes";

@Component({
  selector: 'dentistapp-root',
  templateUrl: './dentistapp.component.html',
  styleUrls: ['./dentistapp.component.scss']
})
export class DentistappComponent implements OnInit {

  title = 'Dentist Appointments';

  constructor(private router: Router) {

  }

  ngOnInit(): void {
    this.router.navigate(['/' + DentistappRoutes.bookingsRoute()])
  }

}
