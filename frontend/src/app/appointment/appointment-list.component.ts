import {Component, OnInit} from "@angular/core";
import {DentistVisit} from "../dentist_visit/dentist-visit";
import {DentistVisitRepository} from "../dentist_visit/dentist-visit.repository";
import {NotificationsService} from "angular2-notifications";
import {Message} from "../messages/message";
import {MatDialog} from "@angular/material/dialog";
import {EditAppointmentModalComponent} from "./modal/edit-appointment-modal.component";
import {DentistVisitItem} from "../dentist/dentist-visit-item";
import * as moment from "moment";
import * as _ from "lodash";
import {Moment} from "moment";

@Component({
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.scss']
})
export class AppointmentListComponent implements OnInit {

  rows: DentistVisitItem[];
  dateComparator: (date: Moment, _date: Moment) => number;

  constructor(private dentistVisitRepository: DentistVisitRepository,
              private notificationService: NotificationsService,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.setDateComparator();
    this.dentistVisitRepository.findAll()
      .then((dentistVisits) => {
        this.rows = [...dentistVisits.map((dentistVisit) => new DentistVisitItem(dentistVisit))];
      })
  }

  edit(row: DentistVisitItem): void {
    const dialogRef = this.dialog.open(EditAppointmentModalComponent,
      {
        data: _.cloneDeep(row)
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      this.refreshList()
        .catch(() => 'Failed to query appointments.');
    });
  }

  refreshList(): Promise<any> {
    return this.dentistVisitRepository.findAll()
      .then((dentistVisits: DentistVisit[]) => {
        this.rows = [...dentistVisits.map((dentistVisit) => new DentistVisitItem(dentistVisit))];
      });
  }

  delete(row: DentistVisitItem): void {
    this.dentistVisitRepository.delete(row.dentistVisit)
      .then(() => this.refreshList())
      .then(() => this.notificationService.success(Message.SUCCESS, 'Booking successfully deleted.'))
      .catch(() => this.notificationService.error(Message.WARNING, 'Failed to delete the booking.'));
  }

  formatDate(row: DentistVisitItem): string {
    return moment(row.dentistVisit.visitDateTime).format("MM/DD/YYYY");
  }

  formatTime(row: DentistVisitItem): string {
    return moment(row.dentistVisit.visitDateTime).format("HH:mm");
  }

  setDateComparator(): void {
    this.dateComparator = function (date: Moment, _date: Moment) {
      if (date.isBefore(_date)) {
        return -1;
      }
      return 1;
    };
  }

}
