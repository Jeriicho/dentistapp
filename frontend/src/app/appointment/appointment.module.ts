import {NgModule} from "@angular/core";
import {AddAppointmentComponent} from "./add-appointment.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {DentistRepository} from "../dentist/dentist.repository";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatInputModule} from "@angular/material/input";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatStepperModule} from "@angular/material/stepper";
import {AppointmentListComponent} from "./appointment-list.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {CommonModule} from "@angular/common";
import {DentistVisitRepository} from "../dentist_visit/dentist-visit.repository";
import {NotificationsService} from "angular2-notifications";
import {EditAppointmentModalComponent} from "./modal/edit-appointment-modal.component";

@NgModule({
  imports: [
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,
    MatGridListModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatButtonModule,
    NgxDatatableModule,
    CommonModule,
    FormsModule
  ],
  declarations: [
    AddAppointmentComponent,
    AppointmentListComponent,
    EditAppointmentModalComponent
  ],
  providers: [
    DentistRepository,
    DentistVisitRepository,
    NotificationsService
  ]
})
export class AppointmentModule {}
