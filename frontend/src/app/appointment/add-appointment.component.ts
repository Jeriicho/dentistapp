import {Component, OnInit, ViewChild} from "@angular/core";
import {DentistRepository} from "../dentist/dentist.repository";
import {Dentist} from "../dentist/dentist";
import {FormBuilder, NgForm} from "@angular/forms";
import * as moment from "moment";
import {Moment} from "moment";
import {DentistVisitRepository} from "../dentist_visit/dentist-visit.repository";
import {DentistVisit} from "../dentist_visit/dentist-visit";
import {NotificationsService} from "angular2-notifications";
import {Message} from "../messages/message";
import {MomentUtil} from "../util/moment-util";
import {MatStepper} from "@angular/material/stepper";
import {Time} from "@angular/common";

@Component({
  templateUrl: './add-appointment.component.html'
})
export class AddAppointmentComponent implements OnInit {

  @ViewChild('form', {static: false}) form: NgForm;
  @ViewChild('stepper') stepper: MatStepper;

  dentist: Dentist;
  dentists: Dentist[] = [];
  date: Moment;
  time: string;

  constructor(private _formBuilder: FormBuilder,
              private dentistRepository: DentistRepository,
              private dentistVisitRepository: DentistVisitRepository,
              private notificationService: NotificationsService) {
  }

  ngOnInit(): void {
    this.dentistRepository.findAll()
      .then((dentists) => this.dentists = dentists);
  }

  currentDate(): Date {
    return moment().toDate();
  }

  private resetForm(): void {
    this.stepper.reset();
    this.form.resetForm();
  }

  submit(): void {
    if (this.form.valid) {
      this.date = moment(MomentUtil.formatDate(this.date));
      this.date = MomentUtil.setHoursAndMinutes(this.date, this.time);
      const data: DentistVisit = new DentistVisit({
        id: undefined,
        dentist: this.dentist,
        visitDateTime: MomentUtil.formatDate(this.date)
      });
      this.dentistRepository.existsOverlappingVisit(data)
        .then((exists: boolean) => {
          if (exists) {
            this.notificationService.error(Message.WARNING, 'An existing appointment with the same time already exists.');
            throw Error()
          }
        })
        .then(() => this.dentistVisitRepository.create(data))
        .then(() => {
          this.notificationService.success(Message.SUCCESS, `Booked an appointment with dentist: ${this.dentist.name}`);
          this.resetForm();
        })
        .catch(() => this.notificationService.error(Message.WARNING, 'Failed to book an appointment.'));
    }
  }

}
