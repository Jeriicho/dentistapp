import {AfterViewChecked, ChangeDetectorRef, Component, Inject, OnInit, ViewChild} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DentistVisitItem} from "../../dentist/dentist-visit-item";
import * as moment from 'moment';
import {DentistRepository} from "../../dentist/dentist.repository";
import {Dentist} from "../../dentist/dentist";
import {NotificationsService} from "angular2-notifications";
import {Message} from "../../messages/message";
import {NgForm} from "@angular/forms";
import {DentistVisitRepository} from "../../dentist_visit/dentist-visit.repository";
import {MomentUtil} from "../../util/moment-util";
import * as _ from "lodash";
import {DentistVisit} from "../../dentist_visit/dentist-visit";
import {Moment} from "moment";

@Component({
  templateUrl: './edit-appointment-modal.component.html'
})
export class EditAppointmentModalComponent implements OnInit, AfterViewChecked {

  @ViewChild('form', {static: false}) form: NgForm;
  dentists: Dentist[] = [];
  dentistVisit: DentistVisit;
  date: Moment;
  time: string;

  constructor(public dialogRef: MatDialogRef<EditAppointmentModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DentistVisitItem,
              private dentistRepository: DentistRepository,
              private dentistVisitRepository: DentistVisitRepository,
              private notificationService: NotificationsService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.dentistRepository.findAll().then((dentists) => {
      this.dentistVisit = new DentistVisit({
        id: this.data.dentistVisit.id,
        dentist: this.data.dentistVisit.dentist,
        visitDateTime : undefined
      });
      this.date = moment(this.data.dentistVisit.visitDateTime);
      this.time = moment(this.data.dentistVisit.visitDateTime).format('HH:mm');
      this.dentists = dentists;
    })
    .catch(() => {

      this.notificationService.error(Message.WARNING, 'Failed to load available dentists.');
    })
  }

  ngAfterViewChecked(): void {
    this.cdr.markForCheck();
    this.cdr.detectChanges();
  }

  currentDate(): Date {
    return moment().toDate();
  }

  close(): void {
    this.dialogRef.close();
  }

  save(): void {
    if (this.form.valid) {
      this.dentistVisit.visitDateTime = MomentUtil.setHoursAndMinutes(this.date, this.time).format('YYYY-MM-DDTHH:mm');
      this.dentistVisitRepository.edit(_.cloneDeep(this.dentistVisit))
        .then(() => {
          this.notificationService.success(Message.SUCCESS, `Appointment with ${this.data.dentistVisit.dentist.name} changed successfully.`);
          this.close();
        })
        .catch(() => this.notificationService.error(Message.WARNING, 'Failed to update the appointment.'));
    }
  }
}
