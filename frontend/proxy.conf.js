/**
 * Proxy configuration for the angular-cli dev server ("yarn serve")
 */

module.exports = {
  "/rest": {
    "target": "http://localhost:8080",
    "secure": false
  }
};
