DROP TABLE IF EXISTS dentist;
DROP TABLE IF EXISTS dentist_visit;

CREATE TABLE dentist (
    id                  IDENTITY        PRIMARY KEY
  , name                VARCHAR2(100)   NOT NULL
);

CREATE TABLE dentist_visit (
    id                  IDENTITY        PRIMARY KEY
  , dentist_id          NUMBER(18)
  , visit_date_time     TIMESTAMP
);

ALTER TABLE dentist_visit
    ADD CONSTRAINT fk_devi_dentist FOREIGN KEY (dentist_id)
        REFERENCES dentist (id);


INSERT INTO dentist (name)
     VALUES ('Toomas Tohter')
          , ('Andrus Arst')
          , ('Denis Dermatoloog');

INSERT INTO dentist_visit (dentist_id, visit_date_time)
     VALUES (
                (
                    SELECT d.id
                      FROM dentist d
                     WHERE d.name = 'Toomas Tohter'
                     LIMIT 1
                ),
                TO_DATE('2020-02-03 12:00', 'YYYY-mm-DD HH:MM')
            ),
            (
                (
                    SELECT d.id
                      FROM dentist d
                     WHERE d.name = 'Andrus Arst'
                     LIMIT 1
                ),
                TO_DATE('2020-02-07 15:00', 'YYYY-mm-DD HH:MM')
            );
