package ee.cgi.dentistapp.dentist_visit.service;

import ee.cgi.dentistapp.dentist.dao.DentistRepository;
import ee.cgi.dentistapp.dentist.mapper.DentistMapper;
import ee.cgi.dentistapp.dentist_visit.DentistVisit;
import ee.cgi.dentistapp.dentist_visit.dao.DentistVisitRepository;
import ee.cgi.dentistapp.dentist_visit.entity.DentistVisitEntity;
import ee.cgi.dentistapp.dentist_visit.mapper.DentistVisitMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DentistVisitServiceImpl implements DentistVisitService {

    private final DentistVisitRepository dentistVisitRepository;

    @Override
    public List<DentistVisit> findAll() {
        return DentistVisitMapper.MAPPER.convert((List<DentistVisitEntity>) this.dentistVisitRepository.findAll());
    }

    @Override
    public boolean isExistsOverlappingVisit(long dentistId, LocalDateTime visitDateTime) {
        return this.dentistVisitRepository.existsOverLappingVisit(dentistId, visitDateTime.minusMinutes(15),
                visitDateTime.plusMinutes(15));
    }

    @Override
    public void create(DentistVisit dentistVisit) {
        if (isExistsOverlappingVisit(dentistVisit.getDentist().getId(), dentistVisit.getVisitDateTime())) {
            throw new IllegalArgumentException("Overlapping visit time.");
        }
        this.dentistVisitRepository.save(DentistVisitMapper.MAPPER.inverse(dentistVisit));
    }

    @Override
    public void update(long id, DentistVisit dentistVisit) {
        if (isExistsOverlappingVisit(dentistVisit.getDentist().getId(), dentistVisit.getVisitDateTime())) {
            throw new IllegalArgumentException("Overlapping visit time.");
        }
        dentistVisit.setId(id);
        this.dentistVisitRepository.save(DentistVisitMapper.MAPPER.inverse(dentistVisit));
    }

    @Override
    public void delete(long id) {
        this.dentistVisitRepository.delete(id);
    }
}
