package ee.cgi.dentistapp.dentist_visit.rest;

import ee.cgi.dentistapp.dentist_visit.DentistVisit;
import ee.cgi.dentistapp.dentist_visit.service.DentistVisitService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@RequiredArgsConstructor
@Path("dentist-visit")
@Produces(MediaType.APPLICATION_JSON)
public class DentistVisitResource {

    private final DentistVisitService dentistVisitService;

    @GET
    public List<DentistVisit> findAll() {
        return dentistVisitService.findAll();
    }

    @POST
    public void createDentistVisit(@Valid DentistVisit dentistVisit) {
        dentistVisitService.create(dentistVisit);
    }

    @PUT
    @Path("{id}")
    public void updateDentistVisit(@PathParam("id") long id, @Valid DentistVisit dentistVisit) {
        dentistVisitService.update(id, dentistVisit);
    }

    @DELETE
    @Path("{id}")
    public void deleteDentistVisit(@PathParam("id") long id) {
        dentistVisitService.delete(id);
    }

}
