package ee.cgi.dentistapp.dentist_visit;

import ee.cgi.dentistapp.dentist.Dentist;
import ee.cgi.dentistapp.common.util.validation.LocalDateTimeInFuture;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class DentistVisit {

    private Long id;

    @NotNull(message = "Dentist cannot be null")
    private Dentist dentist;

    @NotNull(message = "Visit date cannot be null")
    @LocalDateTimeInFuture(message = "Visit date must be in the future.")
    private LocalDateTime visitDateTime;


}
