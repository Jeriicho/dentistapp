package ee.cgi.dentistapp.dentist_visit.mapper;

import ee.cgi.dentistapp.dentist.mapper.DentistMapper;
import ee.cgi.dentistapp.dentist_visit.DentistVisit;
import ee.cgi.dentistapp.dentist_visit.entity.DentistVisitEntity;
import ee.cgi.dentistapp.common.util.mapper.TwoWayMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = DentistMapper.class)
public interface DentistVisitMapper extends TwoWayMapper<DentistVisitEntity, DentistVisit> {

    DentistVisitMapper MAPPER = Mappers.getMapper(DentistVisitMapper.class);

    @Override
    DentistVisit convert(DentistVisitEntity dentistVisitEntity);

    @Override
    @InheritInverseConfiguration
    DentistVisitEntity inverse(DentistVisit dentistVisit);

}
