package ee.cgi.dentistapp.dentist_visit.entity;

import ee.cgi.dentistapp.dentist.entity.DentistEntity;
import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Entity
@Table(name = "dentist_visit")
public class DentistVisitEntity {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dentist_id")
    @NotNull(message = "Dentist cannot be null")
    private DentistEntity dentist;

    @NotNull(message = "Visit Date cannot be null")
    private LocalDateTime visitDateTime;

}
