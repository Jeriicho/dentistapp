package ee.cgi.dentistapp.dentist_visit.service;

import ee.cgi.dentistapp.dentist_visit.DentistVisit;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.List;

public interface DentistVisitService {

    List<DentistVisit> findAll();

    boolean isExistsOverlappingVisit(long dentistId, LocalDateTime visitDateTime);

    void create(DentistVisit dentistVisit);

    void update(long id, DentistVisit dentistVisit);

    void delete(long id);

}
