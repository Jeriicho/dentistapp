package ee.cgi.dentistapp.dentist_visit.dao;

import ee.cgi.dentistapp.dentist_visit.entity.DentistVisitEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface DentistVisitRepository extends CrudRepository<DentistVisitEntity, Long> {

    @Query(value = "SELECT CASE WHEN COUNT(devi) > 0 THEN true ELSE false END FROM DentistVisitEntity devi " +
            "WHERE devi.dentist.id = :dentistId AND devi.visitDateTime BETWEEN :startDateTime AND :endDateTime")
    boolean existsOverLappingVisit(@Param("dentistId") long dentistId,
                                   @Param("startDateTime") LocalDateTime startDateTime,
                                   @Param("endDateTime") LocalDateTime endDateTime);

}
