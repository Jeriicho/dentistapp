package ee.cgi.dentistapp.dentist.entity;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "dentist")
@Getter
@Setter
public class DentistEntity {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 1, max = 50)
    @NotBlank(message = "Dentist name cannot be blank.")
    private String name;

}
