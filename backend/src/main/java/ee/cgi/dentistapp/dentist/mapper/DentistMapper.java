package ee.cgi.dentistapp.dentist.mapper;

import ee.cgi.dentistapp.dentist.Dentist;
import ee.cgi.dentistapp.dentist.entity.DentistEntity;
import ee.cgi.dentistapp.common.util.mapper.TwoWayMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DentistMapper extends TwoWayMapper<DentistEntity, Dentist> {

    DentistMapper MAPPER = Mappers.getMapper(DentistMapper.class);

    @Override
    Dentist convert(DentistEntity dentistEntity);

    @Override
    @InheritInverseConfiguration
    DentistEntity inverse(Dentist dentist);

}
