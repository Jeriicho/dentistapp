package ee.cgi.dentistapp.dentist;

import lombok.*;

@Getter
@Setter
public class Dentist {

    private Long id;
    private String name;

}
