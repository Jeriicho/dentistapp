package ee.cgi.dentistapp.dentist.dao;

import ee.cgi.dentistapp.dentist.entity.DentistEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DentistRepository extends CrudRepository<DentistEntity, Long> {

}
