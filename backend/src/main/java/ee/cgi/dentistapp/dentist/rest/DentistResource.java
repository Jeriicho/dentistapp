package ee.cgi.dentistapp.dentist.rest;

import ee.cgi.dentistapp.dentist.Dentist;
import ee.cgi.dentistapp.dentist.service.DentistService;
import ee.cgi.dentistapp.dentist_visit.service.DentistVisitService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
@Path("dentist")
@Produces(MediaType.APPLICATION_JSON)
public class DentistResource {

    private final DentistService dentistService;
    private final DentistVisitService dentistVisitService;

    @GET
    public List<Dentist> findDentists() {
        return dentistService.findAll();
    }

    @GET
    @Path("{id}/exists-overlap")
    public boolean existsOverlappingDentistVisit(@PathParam("id") long dentistId,
                                                 @QueryParam("visitDateTime") LocalDateTime visitDateTime) {
        return dentistVisitService.isExistsOverlappingVisit(dentistId, visitDateTime);
    }

}
