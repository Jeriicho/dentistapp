package ee.cgi.dentistapp.dentist.service;

import ee.cgi.dentistapp.dentist.Dentist;
import ee.cgi.dentistapp.dentist.dao.DentistRepository;
import ee.cgi.dentistapp.dentist.entity.DentistEntity;
import ee.cgi.dentistapp.dentist.mapper.DentistMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DentistServiceImpl implements DentistService {

    private final DentistRepository dentistRepository;

    public List<Dentist> findAll() {
        return DentistMapper.MAPPER.convert((List<DentistEntity>) dentistRepository.findAll());
    }

}
