package ee.cgi.dentistapp.dentist.service;

import ee.cgi.dentistapp.dentist.Dentist;

import java.util.List;
import java.util.Optional;

public interface DentistService {

    List<Dentist> findAll();

}
