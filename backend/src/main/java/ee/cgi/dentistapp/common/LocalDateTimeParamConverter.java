package ee.cgi.dentistapp.common;

import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Provider
public class LocalDateTimeParamConverter implements ParamConverterProvider {

    private static final DateTimeFormatter CLASSIC_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
        if (rawType == LocalDateTime.class) {
            //noinspection unchecked
            return (ParamConverter<T>) new Java8LocalDateClassicFormatParamConverter();
        }
        return null;
    }


    private static class Java8LocalDateClassicFormatParamConverter implements ParamConverter<LocalDateTime> {

        @Override
        public LocalDateTime fromString(String value) {
            return Optional.ofNullable(value)
                    .map(CLASSIC_DATE_FORMATTER::parse)
                    .map(LocalDateTime::from)
                    .orElse(null);
        }

        @Override
        public String toString(LocalDateTime value) {
            return CLASSIC_DATE_FORMATTER.format(value);
        }
    }

}
