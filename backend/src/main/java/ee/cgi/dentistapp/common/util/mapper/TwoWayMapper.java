package ee.cgi.dentistapp.common.util.mapper;

public interface TwoWayMapper<SOURCE, TARGET> extends OneWayMapper<SOURCE, TARGET>, OneWayInverseMapper<SOURCE, TARGET> {
}

