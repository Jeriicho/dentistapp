package ee.cgi.dentistapp.common.util.mapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface OneWayInverseMapper<SOURCE, TARGET> {

    SOURCE inverse(TARGET target);

    default List<SOURCE> inverse(Collection<TARGET> collection) {
        if (collection == null) {
            return null;
        }
        return collection.stream().map(this::inverse).collect(Collectors.toList());
    }

}
