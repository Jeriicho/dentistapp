package ee.cgi.dentistapp.common.util.mapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface OneWayMapper<SOURCE, TARGET> {

    TARGET convert(SOURCE source);

    default List<TARGET> convert(Collection<SOURCE> collection) {
        if (collection == null) {
            return null;
        }
        return collection.stream().map(this::convert).collect(Collectors.toList());
    }

}
