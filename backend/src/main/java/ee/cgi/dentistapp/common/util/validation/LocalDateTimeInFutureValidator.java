package ee.cgi.dentistapp.common.util.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;

public class LocalDateTimeInFutureValidator implements ConstraintValidator<LocalDateTimeInFuture, LocalDateTime> {

    @Override
    public boolean isValid(LocalDateTime dateTime, ConstraintValidatorContext context) {
        return dateTime != null && LocalDateTime.now().isBefore(dateTime);
    }

}
