package ee.cgi.dentistapp;

import com.fasterxml.jackson.annotation.JsonFormat;
import ee.cgi.dentistapp.common.LocalDateTimeParamConverter;
import org.glassfish.jersey.server.ResourceConfig;
import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Path;
import javax.ws.rs.ext.Provider;

@ApplicationPath("/rest")
@Configuration
@JsonFormat(pattern = "yyyy-MM-ddTHH:mm:ss")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig(@Autowired ApplicationContext applicationContext) {
        applicationContext.getBeansWithAnnotation(Path.class).forEach((name, bean) -> register(bean));
        register(LocalDateTimeParamConverter.class);
    }

    @Bean
    ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
        registrationBean.addUrlMappings("/console/*");
        return registrationBean;
    }

}
