# Dentist Application

## Project
The project itself is composed of 2 child maven projects, `frontend` and `backend`.
Back-end running on spring-boot and front-end on Angular. Since node has its own package management system `package.json`, 
I did not incorporate maven with the `frontend` project due to not wanting to use any maven plugins for solving the projects dependencies.
So `frontend` project having a pom is purely decorative.

In a real-world environment both of those projects should probably have their own dedicated repository and pipelines set up.

##### Why two projects?
Separation of concerns, spring-boot application is no longer an MVC app.  Makes the project overall more scalable.
###### Back-end
* Basically a CRUD app, using JAX-RS api for its back-end endpoints.
###### Front-end
* A regular Angular application with its `Repository` requests proxied to localhost port 8080, where tomcat is running.
## Running
Both projects need to be run separately, because maven is not actually used to build the front-end.
###### Back-end
* cd to [PROJECT_ROOT]/backend
* `mvn spring-boot:run`

###### Front-end
* cd to [PROJECT_ROOT]/frontend
* on initial setup: `npm install`
* `npm run start`

## Overall progress and description
Since I have experience in developing with both technologies, but not much in setting them up initially, 
much of the development time went into configuration. Also reading / practicing with some solutions 
like bean validation and angular material.

Total time spent of project: around one and a half working days. Roughly half of the time spent went on reading additinal documentation. 
###### Encountered problems
* I used Lombok annotation lib for generating getters, setters and constructors - 
this did not work out-of-the-box with org.mapstruct, which I used for mapping entities to DTO-s..
* Encountered a bug in Angular material, where I couldn't set the initial value of a form field. 
(I could set it, but it wasn't displayed in the UI)

###### Things to note
* If editing an appointment. only date and time are editable. 


## Lackings
* NO TESTS AT ALL. Very bad, at least dao methods on the service layers should obviously be covered, but unfortunately 
I did not have any more resources to spend on this project.
* Visit is begging for its own table, but since the only property it currently has is `visitDateTime`, I decided to keep it as-is.
 In a real-world scenario, Dentist and Visit would have a many-to-many relationship with dentist_visit being the junction object/table.
* Check for existing (overlapping) appointments should be done in a different manner, the user should eventually see whether the time is taken before he/she tries to book an appointment. Essentially having a calendar there.
* (Regarding the last point) - boolean endpoints should probably not return true/false, instead the client should interpret the response based on the http status code (200 - true, 404 - false).
* Step 5 (searching in the registration view) is not implemented due to lack of time and not having a clear picture in mind of what is the requirement. 
The table of existing appointments is sortable  on two columns (name, visit time) and I did not know where to put the search column so that it would not look hideous.
* Since I used `momentjs` for representing dates in front-end, there should be a mapper in place which maps the `LocalDateTime` format in `DentistVisitResource` responses to `Moment`, like in back-end dao requests. 
Instead, it's done in the component level. Although, as I read mid-project, since 2020 js's native `Date` should also be okay to use. 
 
